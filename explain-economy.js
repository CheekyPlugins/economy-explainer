var elements = document.getElementsByTagName('*');

for (var i = 0; i < elements.length; i++) { 
  var element = elements[i];

  for (var j = 0; j < element.childNodes.length; j++) {
    var node = element.childNodes[j];

    if (node.nodeType === 3) {
      var text = node.nodeValue;
      var replacedText1 = text.replace(/economy/g, 'yacht money for rich people');
      var replacedText2 = replacedText1.replace(/Economy/g, 'Yacht Money for Rich People');

      if (replacedText2 !== text) {
        element.replaceChild(document.createTextNode(replacedText2), node);
      }
    }
  } 
}
